from enum import Enum
from dataclasses import dataclass

class TransactionType(Enum):
    DEPOSIT = 1
    TRANSFER = 2

@dataclass
class Transaction:
    type: TransactionType
    amount: int
    receiver_account: str
    sender_account: str = None