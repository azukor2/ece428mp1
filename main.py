#!/bin/python3

import sys
import threading
from TransactionDeliverer import TransactionDeliverer
from SelectServer import Server
from Messages import TransactionMessage
from Transaction import Transaction, TransactionType
from signal import SIGINT, signal
import time

is_running = True

def signal_handler(sig, frame):
    is_running = False

signal(SIGINT, signal_handler)

def process_config(config_file):
    with open(config_file, 'r') as file:
        num_nodes = int(file.readline())
        client_names = []
        addresses = []
        ports = []
        for _ in range(num_nodes):
            (node_name, address, port) = file.readline().split(' ')
            port = int(port)
            if node_name == identifier:
                # We want the running node's details to be in position 0
                client_names.insert(0, node_name)
                addresses.insert(0, address)
                ports.insert(0, port)
            else:
                client_names.append(node_name)
                addresses.append(address)
                ports.append(port)

    return client_names, addresses, ports

def process_inputs(td: TransactionDeliverer):
    for line in sys.stdin:
        if not is_running:
            return

        line = line.split(' ')
        if line[0] == "DEPOSIT":
            transaction = Transaction(TransactionType.DEPOSIT, int(line[2].strip()), line[1])
        elif line[0] == "TRANSFER":
            transaction = Transaction(TransactionType.TRANSFER, int(line[4].strip()), line[3], line[1])
        else:
            # Should never happen!
            raise NotImplementedError("Unknown command")

        with td.td_lock:
            td.new_transaction_message(transaction)

def clear_timeout_loop(td):
    while True:
        with td.td_lock:
            td.check_timeouts()
        time.sleep(td.TIMEOUT)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(f"Help: {sys.argv[0]} <identifier> <configuration file>")
    else:
        identifier = sys.argv[1]
        config_file = sys.argv[2]

        logfile = None
        if len(sys.argv) >= 4:
            logfile = open(sys.argv[3], 'w')

        client_names, addresses, ports = process_config(config_file) 
        td = TransactionDeliverer(client_names, addresses, ports, logfile)

        server_thread = Server(td, addresses[0], ports[0], len(addresses) - 1)
        server_thread.start()

        timeouts_thread = threading.Thread(target=clear_timeout_loop, args=(td,))
        timeouts_thread.daemon = True
        timeouts_thread.start()

        td.bring_up_clients()
        process_inputs(td)

        server_thread.end.set()
        server_thread.join()

        if logfile:
            logfile.close()